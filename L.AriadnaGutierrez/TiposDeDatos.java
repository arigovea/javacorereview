package com.company;

public class TiposDeDatos {

    public static void main(String[] args) {

        byte numberB = 122;
        short numberS = 3600;
        long numberL = 15186135;
        float numberF = 1.20252f;

        System.out.println(numberB);
        System.out.println(numberS);
        System.out.println(numberL);
        System.out.println(String.format("%.2f", numberF));

        System.out.println("numberB es mayor que numberS? " + (numberB > numberS));
        System.out.println("numberF es menor que numberL? " + (numberF < numberL));
        System.out.println("numberL es mayor o igual que numberB? " + (numberL >= numberB));
        System.out.println("numberS es menor o igual que numberF? " + (numberS <= numberF));
        System.out.println("numberB es distinto a numberF? " + (numberB != numberF));
        System.out.println("numberS es igual a numberL? " + (numberS == numberL));

        int exampleAndOr = 35;
        int min = 0, max = 30;
        boolean resultAnd = exampleAndOr >= min && exampleAndOr <= max;
        boolean resultOr = exampleAndOr >= min || exampleAndOr <= max;
        System.out.println("Resultado de AND: " + resultAnd);
        System.out.println("Resultado de OR: " + resultOr);

        /*
        El papel de los operadores AND y OR es evaluar sentencias (condiciones) que nos den
        como resultado un boolean.
        En este ejemplo, se va a comparar un número con un valor mínimo y uno máximo.
        Como podemos observar en los resultados cuando hacemos una comparación AND, nos da un false
        si el número a comparar se sale de alguno de los dos rangos, ya que para poder dar un true estrictamente
        todas las condiciones se deben de cumplir. En este caso 35 sí es mayor o igual que 0,
        pero no es menor que 30.
        Por el lado contrario, con el OR obtenemos un true, porque para poder otener éste, solo necesitamos
        que alguna de las dos se cumpla, sin importar si la otra fue false.
        Si ambas condiciones se cumplieran tanto en AND como en OR, obtendríamos un true.
        Si la primera condición fuera false en el caso de AND, no haría falta evaluar la segunda condición porque ya
        no se cumplen las dos.
        Si la primera condición fuera true en el caso de OR (como en este caso),
        no hace falta evuluar la segunda condición porque ya se cumplió al menos una.
         */

        int a = 15;
        int b = 15;
        int c = a & b;
        System.out.println(c);

        /* SpreadOperator
        Intenté hacer algo como el ejemplo de Oscar pero no resultó y tampoco quería copiarlo,
        tengo más experencia usando JavaScript que Java así que intenté importar un código de JS a Java
        pero tampoco resultó así que este es mi código de JS.

        var spreadOperator = function () {
	        let array1 = [0, 1, 2, 3, 4];
            let array2 = [5, 6, 7, 8, 9];
	        array1.push(...array2);
	        print(array1);
        };

        El spread Operator se usa para "transferir" o "copiar" los datos de un array a otro o a una función sin que
        estén anidados. Un ejemplo muy útil de cuándo usarlo sería cuando queremos modificar los valores
        de un array pero queremos conservar los valores originales, con el SO hacemos una copia de ese array
        para que esta sea modificada y no la original.

         */

    }
}