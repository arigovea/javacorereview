package com.company;

public class session3 {
    public static void main(String[] args) {

        String name = "Ari";
        String day = "Wednesday";

        sayHi();
        System.out.println(sayHi(name));
        System.out.println(sayHi(name, day));
    }

    public static void sayHi() {
        System.out.println("Hellooo!");
    }

    public static String sayHi(String name) {
        return "Hellooo! " + name;
    }

    public static String sayHi(String name, String day) {
        return "Hellooo! " + name + ", today is " + day;
    }

    /*
    Concepto Static:

    Un método y una variable static hacen referencia a que no se necesita
    isntanciar un objeto para poder hacer uso de ellos, en otras palabras,
    le pertenece a la clase y no a los objetos que se puedan crear de ésta.
    Resulta muy práctico cuando queremos que ese dato o método afecte a
    todos los objetos, y en lugar de tener que modificar uno por uno, al
    cambiar éste, se cambian todos.
    No es bueno que sea usado justamente cuando sean datos propios de cada
    objeto, por ejemplo, la clase estudiantes que tenga una variable nombre,
    este no debe ser static porque cada persona tiene uno y es único, si se
    modifica (por algún error) se modificarían todos los nombres; pero si
    tuvieramos un id, necesitariamos static porque a pesar de que sería único,
    el static se usaría para que el programa supiera que id se debe ingresar
    con un nuevo estudiante sin hacerlo manual o recurrir a revisar el último
    estudiante.
     */
}
